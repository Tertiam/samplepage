import { Component, Input } from '@angular/core';
@Component({
    selector: 'app-edit-area',
    templateUrl: './editArea.component.html',
    styleUrls: ['./editArea.css']
  })
  export class EditAreaComponent {
      @Input() inputValue = '';
      @Input() inputValue2 = '';
      outputValue = '';
    constructor() { }

    update(value: string) {
        this.outputValue = 'The inputted value is: ' + value;
    }

  }
